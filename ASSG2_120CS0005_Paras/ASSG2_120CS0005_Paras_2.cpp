#include<bits/stdc++.h>
using namespace std;

unordered_map<string, string> productions;
unordered_map<string, string> first;
unordered_map<string, string> follow;

bool containsEpsillon(string key){
    if(first.find(key) == first.end())
        return false;
    string f = first[key];
    for(int i = 0; i < f.length(); i++){
        if(f[i] == '^')
            return true;
    }
    return false;
}
string removeDuplicatesAndEpsillon(string s){
    unordered_set<char> hs;
    string ans = "";
    for(char c : s){
        if(c != '^' && hs.find(c) == hs.end()){
            ans += c;
            hs.insert(c);
        }
    }
    return ans;
}
string findFirst(string lhs){
    string rhs = productions[lhs];
    string ans = "";
    if(first.find(lhs) != first.end())
        return first[lhs];
    for(int i = 0; i < rhs.length(); i++){
        if(i == 0 || rhs[i - 1] == '|'){
            if(rhs[i] >= 'A' && rhs[i] <= 'Z'){
                ans += findFirst(string(1, rhs[i]));
            } else{
                ans += rhs[i];
            }
        }
    }
    return ans;
}
void findFirsts(int numberOfProductions){
    while(first.size() != numberOfProductions){
        for(auto [key, val] : productions){
            if(first.find(key) == first.end()){
                first[key] = findFirst(key);
            }
        }
    }
    for(auto [key, val] : first){
        cout << "FIRST(" << key << "): { ";
        for(char c : val)
            cout << c <<", ";
        cout << "} " << endl;
    }
}
string findFollow(string x){
    string ans = "";
    if(follow.find(x) != follow.end())
        return follow[x];
    for(auto [key, val] : productions){
        for(int i = 0; i < val.length(); i++){
            if(val[i] == x[0]){
                bool flag = true;
                if(i < val.length() - 1){
                    i++;
                    while(i < val.length() && flag && val[i] != '|'){
                        if(val[i] >= 'A' && val[i] <= 'Z'){
                            ans += first[string(1, val[i])];
                        }
                        else{
                            ans += val[i];
                        }
                        if(!containsEpsillon(string(1, val[i])))
                            flag = false;
                        i++;
                    }
                    if(flag && x[0] != key[0]){
                        ans += findFollow(key);
                    }
                } else if(key[0] != x[0]){
                    ans += findFollow(key);
                }
            }
        }
    }
    ans = removeDuplicatesAndEpsillon(ans);
    if(x == "E")
        ans += "$";
    return ans;
}
void findFollows(int numberOfProductions){
    while(follow.size() != numberOfProductions){
        for(auto [key, val] : productions){
            for(int i = 0; i < val.length(); i++){
                if(val[i] >= 'A' && val[i]<= 'Z'){
                    follow[string(1, val[i])] = findFollow(string(1, val[i]));
                }
            }
        }
    }
    for(auto [key, val] : follow){
        cout << "FOLLOW(" << key << "): { ";
        for(char c : val)
            cout << c <<", ";
        cout << "} " << endl;
    }
}
int main(){
    int n;
    cout << "Enter number of productions\n";
    cin >> n;
    for(int i = 0; i < n; i++){
        string s;
        cin >> s;
        productions[s.substr(0, 1)] = s.substr(3);
    }
    
    findFirsts(n);
    cout << endl;
    findFollows(n);
    return 0;
}


// 5
// E->TA
// A->+TA|^
// T->FB
// B->*FB|^
// F->(E)|is

// 5
// E->TA
// A->+TA|^
// T->FB
// B->*FB|^
// F->t|(E)