#include<bits/stdc++.h>
using namespace std;

string s;
int ind;

bool A(){
    if(s[ind++] == 'a'){
        if(ind == s.length() - 2 && s[ind] == 'b'){
            ind++;
            return true;
        }
        else if(ind == s.length() - 1)
            return true;
        else
            return false;
    } 
    return false;
}

bool S(){
    if(s[ind++] == 'c' && A() && ind == s.length() - 1 && s[ind++] == 'd')
        return true;
    return false;
}

int main(){
    cin >> s;
    ind = 0;

    if(S()){
        cout << "The String is accepted" << endl;
    } else{
        cout << "The String is not accepted" << endl;
    }
    return 0;
}