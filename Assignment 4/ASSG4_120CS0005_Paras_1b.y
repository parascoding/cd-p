 %{
    #include<stdio.h>
    int isValid=0;
   
%}
%token NUMBER ID

%left '+' '-'
%left '*' '/' '%'
%left '(' ')'
%%
ArithmeticExpression: E{
         printf("\nResult is = %d\n",$$);
         return 0;
        }
E:E'+'E {$$=$1+$3;}
 |E'-'E {$$=$1-$3;}
 |E'*'E {$$=$1*$3;}
 |E'/'E {$$=$1/$3;}
 |E'%'E {$$=$1%$3;}
 |'('E')' {$$=$2;}
 | NUMBER {$$=$1;}
;
%%

void main(){
   printf("\nEnter Arithmetic Expression\n");
   yyparse();
  if(isValid==0)
   printf("\nEntered expression is Valid\n\n");
 
}
void yyerror()
{
   printf("\nEntered arithmetic expression is Invalid\n\n");
   isValid = 1;
}

