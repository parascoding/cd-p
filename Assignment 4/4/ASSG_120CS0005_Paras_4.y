%{
#include <math.h>
#include<ctype.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct tree_node
{
 char val[10];
 int lc;
 int rc;
};

int ind;
struct tree_node syntaxTree[100];

void printTree(int cur_ind);
int makeNode(int lc,int rc,char val[10]);
%}

%token digit

%%

S: E { printTree($1); };

E: E '+' T { $$ = makeNode($1,$3,"+"); }
 | E '-' T { $$ = makeNode($1,$3,"-"); }
 | T { $$ = $1; };

T: T '*' F { $$ = makeNode($1,$3,"*"); }
 | T '/' F { $$ = makeNode($1,$3,"/"); }
 | F { $$ = $1; };

F: P '^' F { $$ = makeNode($1,$3,"^"); }
 | P { $$ = $1; };

P: '(' E ')' { $$ = $2; }
 | digit { char buf[10]; sprintf(buf,"%d", yylval); $$ = makeNode(-1,-1,buf); };

%%

int main()
{
    ind = 0;
    printf("Enter an expression\n");
    yyparse();
    return 0;
}

void yyerror()
{
    printf("Error\n");
}

int makeNode(int lc,int rc,char val[10])
{
    strcpy(syntaxTree[ind].val,val);
    syntaxTree[ind].lc = lc;
    syntaxTree[ind].rc = rc;
    ind++;
    return ind-1;
}

void printTree(int cur_ind)
{
    if(cur_ind == -1) return;
    if(syntaxTree[cur_ind].lc == -1 && syntaxTree[cur_ind].rc == -1)
        printf("Digit Node -> Index : %d, Value : %s \n",cur_ind,syntaxTree[cur_ind].val);
    else
        printf("Operator Node -> Index : %d, Value : %s, Left Child Index : %d, Right Child Index : %d \n",cur_ind,syntaxTree[cur_ind].val, syntaxTree[cur_ind].lc, syntaxTree[cur_ind].rc);

    printTree(syntaxTree[cur_ind].lc);
    printTree(syntaxTree[cur_ind].rc);
}
