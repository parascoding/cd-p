%{
#include<stdio.h>
%}
%%
[0-9]*\.[0-9]+ printf("%s is a floating point number\n", yytext);
.* printf("%s is not a floating point number\n", yytext);
%%
int yywrap(void)
{
return 1;
}
int main()
{
printf("Enter any number\n");
yylex();
return 0;
}