%{
#include<stdio.h>
#include "y.tab.h"
extern float yylval;
%}

%%
[0-9]+ {
          yylval=atof(yytext);
          return FLOAT;
       }
[\t] ;
[\n] return 0;
. return yytext[0];
%%
int yywrap()
{
return 1;
}