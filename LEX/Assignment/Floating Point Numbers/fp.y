 %{
    #include<stdio.h>
    int flag=0;
   
%}
%token FLOAT

%left '+' '-'
%left '*' '/' '%'
%left '(' ')'
%%
ArithmeticExpression: E{
         printf("\nResult is = %d\n",$$);
         return 0;
        }
E:E'+'E {$$=$1+$3;}
 |E'-'E {$$=$1-$3;}
 |E'*'E {$$=$1*$3;}
 |E'/'E {$$=$1/$3;}
 |E'%'E {$$=$1%$3;}
 |'('E')' {$$=$2;}
 | FLOAT {$$=$1;}
;
%%
void main()
{
   printf("\nEnter Floating Point Number\n");
   yyparse();
  if(flag==0)
   printf("\nEntered numer is Valid\n\n");
 
}
void yyerror()
{
   printf("\nEntered number is Invalid\n\n");
   flag = 1;
}

