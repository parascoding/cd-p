import java.io.*;
import java.util.*;

public class Main{
    static BufferedReader br;
    static PrintWriter ot;
    public static void main(String args[]) throws IOException{
        br = new BufferedReader(new FileReader("Temp.cpp"));
        ot = new PrintWriter(System.out);
        String x;
        pre();
        StringBuilder result = new StringBuilder();
        boolean stringFlag = false;
        boolean commentFlag = false;
        Map<String, Integer> hm = new HashMap<>();
        while((x = br.readLine()) != null){
            String s[] = x.trim().split(" ");
            if(s.length == 0)
                continue;
            for(int i = 0; i < s.length; i++){
                if(isCommentStarting(s[i])){
                    commentFlag = true; 
                    hm.put("COMMENTS", hm.getOrDefault("COMMENTS", 0) + 1);
                    continue;
                }
                if(isCommentEnding(s[i])){
                    commentFlag = false;
                    continue;
                }
                if(commentFlag){
                    continue;
                }
                if(isStringStarting(s[i])){
                    stringFlag = true;
                    hm.put("STRINGS", hm.getOrDefault("STRINGS", 0) + 1);
                }
                if(isComment(s[i])){
                    hm.put("COMMENTS", hm.getOrDefault("COMMENTS", 0) + 1);
                    break;
                }
                if(stringFlag){
                    result.append(s[i]).append(" is a String\n");
                    if(isStringEnding(s[i]))
                        stringFlag = false;
                }
                else if(isKeyword(s[i])){
                    result.append(s[i]).append(" is a keyword\n");
                    hm.put("KEYWORDS", hm.getOrDefault("KEYWORDS", 0) + 1);
                }
                else if(isIdentifier(s[i], s[i].length(), 0)){
                    result.append(s[i]).append(" is an indentifier\n");
                    hm.put("IDENTIFIERS", hm.getOrDefault("IDENTIFIERS", 0) + 1);
                }
                else if(isOperator(s[i])){
                    result.append(s[i]).append(" is an operator\n");
                    hm.put("OPERATORS", hm.getOrDefault("OPERATORS", 0) + 1);
                }
                else if(isIntegerOrFloat(s[i])){
                    result.append(s[i]).append(" is a Real Number\n");
                    hm.put("NUMBERS", hm.getOrDefault("NUMBERS", 0) + 1);
                }
                else if(isHeader(s[i])){
                    result.append(s[i]).append(" is a Header File\n");
                    hm.put("HEADERS", hm.getOrDefault("HEADERS", 0) + 1);
                }
                else if(isTerminator(s[i])){
                    result.append(s[i]).append(" is a Terminator\n");
                    hm.put("TERMINATORS", hm.getOrDefault("TERMINATORS", 0) + 1);
                } 
                else if(isBraces(s[i])){
                    result.append(s[i]).append(" is a Brace\n");
                    hm.put("BRACES", hm.getOrDefault("BRACES", 0) + 1);
                }
                else{
                    result.append(s[i]).append(" is a Invalid\n");
                    hm.put("INVALID", hm.getOrDefault("INVALID", 0) + 1);
                }
            }            
        }
        ot.println(result);
        for(Map.Entry<String, Integer> e : hm.entrySet()){
            ot.println(e.getKey() + " " + e.getValue());
        }
        ot.close();
    }
    static boolean isCommentStarting(String s){
        return s.equals("/*");
    }
    static boolean isCommentEnding(String s){
        return s.equals("*/");
    }
    static boolean isComment(String s){
        return s.equals("//");
    }
    static boolean isBraces(String s){
        return s.length() == 1 && 
            (s.charAt(0) == '(') ||
            (s.charAt(0) == ')') ||
            (s.charAt(0) == '[') ||
            (s.charAt(0) == ']') ||
            (s.charAt(0) == '{') ||
            (s.charAt(0) == '}');
    }
    static boolean isTerminator(String s){
        return s.length() == 1 && s.charAt(0) == ';';
    }
    static boolean isStringEnding(String s){
        return s.charAt(s.length() - 1) == '"';
    }
    static boolean isStringStarting(String s){
        if(s.length() == 0)
            return false;
        return s.charAt(0) == '"';
    }
    static boolean isHeader(String s){
        return s.startsWith("#include<");
    }
    static boolean isIntegerOrFloat(String s){
        if(s.length() == 0)
            return true;
        if(isNumber(s.charAt(0)) || isDecimal(s.charAt(0)))
            return isIntegerOrFloat(s.substring(1));
        return false;
    }
    static boolean isNumber(char c){
        return c >= '0' && c <= '9';
    }
    static boolean isDecimal(char c){
        return c == '.';
    }
    static boolean isOperator(String s){
        return operators.contains(s);
    }
    static boolean isIdentifier(String s, int n, int ind){
        if(ind == n)
            return true;
        if(ind == 0 && !isCharacter(s.charAt(0)))
            return false;
        return isIdentifier(s, n, ind + 1);
    }
    static boolean isCharacter(char c){
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }
    static boolean isKeyword(String s){
        return keywords.contains(s);
    }
    static void pre(){
        keywords = new HashSet<>();
        operators = new HashSet<>();
        keywords.add("asms");
        keywords.add("double");
        keywords.add("new");
        keywords.add("switch");
        keywords.add("auto");
        keywords.add("else");
        keywords.add("operator");
        keywords.add("template");
        keywords.add("break");
        keywords.add("enum");
        keywords.add("private");
        keywords.add("this");
        keywords.add("case");
        keywords.add("extern");
        keywords.add("protected");
        keywords.add("throw");
        keywords.add("catch");
        keywords.add("float");
        keywords.add("public");
        keywords.add("try");
        keywords.add("char");
        keywords.add("for");
        keywords.add("register");
        keywords.add("typedef");
        keywords.add("class");
        keywords.add("friend");
        keywords.add("return");
        keywords.add("union");
        keywords.add("const");
        keywords.add("goto");
        keywords.add("short");
        keywords.add("unsigned");
        keywords.add("continue");
        keywords.add("if");
        keywords.add("signed");
        keywords.add("virtual");
        keywords.add("default");
        keywords.add("inline");
        keywords.add("sizeof");
        keywords.add("void");
        keywords.add("delete");
        keywords.add("int");
        keywords.add("static");
        keywords.add("volatile");
        keywords.add("do");
        keywords.add("long");
        keywords.add("struct");
        keywords.add("while");
        operators.add("+");
        operators.add("-");
        operators.add("*");
        operators.add("/");
        operators.add("%");
        operators.add("++");
        operators.add("--");
        operators.add("=");
        operators.add("==");
        operators.add("!=");
        operators.add(">");
        operators.add(">");
        operators.add(">=");
        operators.add("<=");
        operators.add("&&");
        operators.add("||");
        operators.add("~");
        operators.add("<<");
        operators.add("^");
        operators.add("!");
        operators.add("&");
        operators.add("|");
        operators.add(">>");
    }
    static Set<String> keywords;
    static Set<String> operators;
    
}
