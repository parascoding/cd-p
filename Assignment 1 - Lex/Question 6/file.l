d [0-9]

%{
    int c1=0,c2=0,c3=0,c4=0,c5=0,c6=0,c7=0;
%}

%%
({d})*00 {
    c1++; 
    printf("%s rule 1\n",yytext);
}

({d})*222({d})* {
    c2++; 
    printf("%s rule 2\n",yytext);
}

(1(0)*(11|01)(01*01|00*10(0)*(11|1))*0)(1|10(0)*(11|01)(01*01|00*10(0)*(11|1))*10)* {
    c4++;
    printf("%s rule 4\n",yytext);
}
 
({d})*1{d}{9} {
    c5++; 
    printf("%s rule 5\n",yytext);
}

{d}{4} {
    int sum=0,i; 
    for(i=0;i<4;i++) {
        sum=sum+yytext[i]-48; 
    }
    if(sum==9) { 
        c6++; 
        printf("%s rule 6\n",yytext);
    }
    else {
        printf("%s doesn't match any rule\n",yytext); 
    }
    
}
({d})* { 
    int i,c=0;
    if(yyleng<5) { 
        printf("%s doesn't match any rule\n",yytext); 
    }
    else {
        for(i=0;i<5;i++) { 
            if(yytext[i]=='5') {
                c++; 
            }
        }
        if(c>=2){
            for(;i<yyleng;i++){
                if(yytext[i-5]=='5') { 
                    c--; 
                }
                if(yytext[i]=='5') { 
                    c++;
                }
                if(c<2) { 
                    printf("%s doesn't match any rule\n",yytext); 
                    break; 
                }
            }
            if(yyleng==i) { 
                printf("%s rule 3\n",yytext); c3++; 
            }
        }
        else {
            printf("%s doesn't match any rule\n",yytext);
        }
    }
}

%%

void main()
{
    yyin = fopen("Input.txt", "r");
    yylex();
    printf("Total number of tokens matching rules are : \n");
    printf("Rule 1 : %d \n",c1);
    printf("Rule 2 : %d \n",c2);
    printf("Rule 3 : %d \n",c3);
    printf("Rule 4 : %d \n",c4);
    printf("Rule 5 : %d \n",c5);
    printf("Rule 6 : %d \n",c6);
}
int yywrap(){
    
}
