c[a-zA-Z]
vowel[aeiouAEIOU]
consonant[^aeiouAEIOU]
%%

{vowel}{c}* {
    char s[200];
    strcpy(s,yytext);
    strcat(s,"ay");
    printf("%s ",s);
    fprintf(yyout,"%s",s);
}

{c}{c}* {

    char s[200];
    strcpy(s,yytext+1);
    printf("%s%cay ",s,yytext[0]);
    fprintf(yyout,"%s%cay",s,yytext[0]);
}

%%

int main(){
    printf("Output : \n");
    yyin = fopen("input.txt","r");
    yyout = fopen("output1.txt","w");
    yylex();

    printf("\n\n\n");

    fclose(yyin);
    fclose(yyout);

    yyin = fopen("output1.txt","r");

    yyout = fopen("output2.txt","w");

    yylex();
    printf("\n");

    return 0;
}