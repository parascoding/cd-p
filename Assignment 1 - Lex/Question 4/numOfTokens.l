%{ 
int n = 0 ;
int keywords = 0;
int identifier = 0;
int operator = 0;
int separator = 0;
int floatC = 0;
int integer = 0;
%}

%%
"while"|"if"|"else" {n++;keywords++;}  
  
"int"|"float" {n++;keywords++;}   

[a-zA-Z_][a-zA-Z0-9_]* {n++;identifier++;} 

"<="|"=="|"="|"++"|"-"|"*"|"+" {n++;operator++;}

[(){}|, ;]    {n++;separator++;} 

[0-9]*"."[0-9]+ {n++;floatC++;}  
  
[0-9]+ {n++;integer++;}                        
. ;
%%   
   
int main() { 
    yyin = fopen("test.txt", "r");     
    yylex();
    printf("Numer of Keywords = %d \n", keywords);
    printf("Numer of Operators = %d \n", operator);
    printf("Numer of Separator = %d \n", separator);
    printf("Numer of Identifier = %d \n", identifier);
    printf("Numer of Constant = %d \n", integer + floatC);
    printf("\n total no. of token = %d\n", n);   
       
}