/* Definitions */
d [0-9]+
a [a-zA-Z]+
%{
/* Including the required header files. */
#include<stdio.h>
#include<stdlib.h>
#include"y.tab.h"
extern int yylval;
extern char iden[20];
%}
/*
 Rules:
 If any number is matched, make it as the yyval and send as token.
 If any word is matched, make it as the yylval and send as token.
 If any delimiter is matched, does nothing about it.
 If a new line character is encountered, end the program.
 If anything else is matched, send the first character of the matched
text.
*/
%%
{d} { yylval=atoi(yytext); return digit; }
{a} { strcpy(iden,yytext); yylval=1; return id; }
[ \t] {;}
\n return 0;
. return yytext[0];
%%