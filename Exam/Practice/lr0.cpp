#include <iostream>
#include <vector>
#include <stack>
#include <unordered_map>
#include <set>

using namespace std;

// Data structures to represent the grammar and parser table
struct Production {
    char lhs;
    string rhs;
};

struct Item {
    Production prod;
    int dot_pos;
};

struct State {
    int state_num;
    vector<Item> items;
};

unordered_map<int, State> states;
unordered_map<char, set<char>> follow_set;
vector<char> symbols;
vector<vector<int>> action_table;
vector<vector<int>> goto_table;

// Functions to compute the closure and goto sets
set<Item> closure(const set<Item>& items, const vector<Production>& grammar) {
    set<Item> closure_items = items;

    while (true) {
        bool added = false;

        for (auto item : closure_items) {
            if (item.dot_pos < item.prod.rhs.size()) {
                char next_sym = item.prod.rhs[item.dot_pos];

                if (isupper(next_sym)) {
                    for (auto prod : grammar) {
                        if (prod.lhs == next_sym) {
                            Item new_item = { prod, 0 };
                            if (closure_items.find(new_item) == closure_items.end()) {
                                closure_items.insert(new_item);
                                added = true;
                            }
                        }
                    }
                }
            }
        }

        if (!added) {
            break;
        }
    }

    return closure_items;
}

set<Item> goto_set(const set<Item>& items, char symbol, const vector<Production>& grammar) {
    set<Item> goto_items;

    for (auto item : items) {
        if (item.dot_pos < item.prod.rhs.size() && item.prod.rhs[item.dot_pos] == symbol) {
            Item new_item = { item.prod, item.dot_pos + 1 };
            goto_items.insert(new_item);
        }
    }

    return closure(goto_items, grammar);
}

// Functions to construct the LR(0) automaton and parse table
void construct_lr0_automaton(const vector<Production>& grammar) {
    int state_num = 0;

    // Compute closure of initial item and add to state 0
    State initial_state = { state_num++, closure({ Item{ grammar[0], 0 } }, grammar) };
    states[0] = initial_state;

    // Process each state in the automaton
    for (int i = 0; i < state_num; i++) {
        State state = states[i];

        // Compute all possible goto sets from this state
        unordered_map<char, set<Item>> goto_map;
        for (auto item : state.items) {
            if (item.dot_pos < item.prod.rhs.size()) {
                char next_sym = item.prod.rhs[item.dot_pos];
                if (goto_map.find(next_sym) == goto_map.end()) {
                    goto_map[next_sym] = goto_set(state.items, next_sym, grammar);
                }
            }
        }

        // Create new states for each non-empty goto set
        for (auto entry : goto_map) {
            set<Item> goto_items = entry.second;
            if (!goto_items.empty()) {
                bool found = false;
                int j;
                for (j = 0; j < state_num; j++) {
                    if (states[j].items == goto_items) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    State new_state = { state_num++, goto_items };
                    states[new_state.state_num] = new_state;
                }

                // Add a transition from this state to the new state
                char symbol = entry.first;
                if (isupper(symbol)) {
                    goto_table[i][symbols.size() - 1 - (grammar.size() - symbol - 1)] = j;
                } else {
                    action_table[i][symbols.size() - 1 - (grammar.size() + symbol - 'a')] = j;
                }
            }
        }
    }
}
int main(){
    
}