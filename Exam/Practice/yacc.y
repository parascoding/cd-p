%{
    #include<stdio.h>
    #include<stdlib.h>
%}

%token NUMBER
%left '+' '-'
%left '*' '/'

%%
S: E{
    printf("\n\n");
}

E: E'+'E {printf("+");}
    | E'-'E {printf("-");}
    | E'*'E {printf("*");}
    | E'/'E {printf("/");}
    | E'^'E {printf("^");}
    | '('E')'
    | NUMBER {printf("%d", $1);}
;

%%

void main(){
    printf("\nEtner expression\n");
    yyparse();

}
void yyerror(){
    printf("\nEntered arithmetic expression is Invalid\n\n");
}