%{
    #include<stdio.h>
    #include<stdlib.h>
%}



%token NUM ID

%left '+' '-'
%left '*' '/'
%left '(' ')'
%%
S: E{
    printf("RES =%d", $$);
}
;

E:  E '+' E {$$ = $1 + $3;}
    | E '-' E {$$ = $1 - $3;}
    | E '*' E {$$ = $1 * $3;}
    | E '/' E {$$ = $1 / $3;}
    | '(' E ')' {$$ = $2;}
    | NUM {$$ = $1;}
;
%%

void main(){
    printf("ENTER\n");
    yyparse();
    
}

void yyerror(){
    printf("ERROR\n");
}