%{
 /* Definition section */
 #include <ctype.h>
 #include<stdio.h>
 #include<stdlib.h>
%}
%token digit
/* Rule Section */
%%
/*After Evaluating the expression E,S prints Reached*/
S: E {printf("Reached\n\n");}
;
/*The expression parser uses three different symbols T,F and P, to
set the
 precedence and associativity of operators*/
E: E '+' T
| E '-' T
| T
;
T: T '*' P
| T '/' P
| P
;
P: F '^' P
| F
;
F: '(' E ')'
 | digit
 ;
%%

//driver code
int main()
{
 printf("Enter infix expression: ");
 yyparse();
}
yyerror()
{
 printf("NITW Error");
} 