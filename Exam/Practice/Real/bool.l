%{
 #include "y.tab.h"
 extern int yylval;
 #include <stdlib.h>
%}
/*
 Rules:
 If 'T' is matched, send it as a token.
 If 'F' is matched, send it as a token.
 If a tab is mathced, do nothing.
 If a new line character is matched, end the parsing.
 For any other word or character, send the first
character as the token.
*/
%%
[T] { yylval=yytext[0]; return digit;}
[F] { yylval=yytext[0]; return digit;}
[\t] ;
[\n] return 0;
. return yytext[0];
%%