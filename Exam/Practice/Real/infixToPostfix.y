%{
    #include<stdio.h>
    #include<stdlib.h>
%}

%token NUMBER

%%
S: E{
    printf("\n");
}
;

E: E'+'T {printf("+");}
    | E'-'T {printf("-");}
    | E'/'T {printf("/");}
    | E'*'T {printf("*");}
    | E'^'T {printf("^");}
    | '('E')' {}
    | T
;
T: NUMBER {printf("%d", $1);}
;
%%

void main(){
    printf("Enter\n");
    yyparse();
}

void yyerror(){
    printf("ERROR");
}