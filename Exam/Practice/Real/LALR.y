%{
 #include "y.tab.h"
 extern int yylval;
%}
%%
//If the token is an Integer number,then return it's value.
[0-9]+ {yylval=atoi(yytext); return digit;}
//If the token is space or tab,then just ignore it.
[\t] ;
//If the token is new line,return 0.
[\n] return 0;
//For any other token, return the first character read since the last
match.
. return yytext[0];
%%