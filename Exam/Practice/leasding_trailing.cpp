set<char> leading(char nt, const vector<Production>& grammar) {
    set<char> lead_set;
    for (auto prod : grammar) {
        if (prod.lhs == nt) {
            if (isupper(prod.rhs[0])) {
                set<char> first_set = first(prod.rhs, grammar);
                lead_set.insert(first_set.begin(), first_set.end());
            } else if (prod.rhs[0] != EPSILON) {
                lead_set.insert(prod.rhs[0]);
            }
        }
    }
    return lead_set;
}

// Compute the trailing set of a non-terminal symbol in a grammar
set<char> trailing(char nt, const vector<Production>& grammar) {
    set<char> trail_set;
    for (auto prod : grammar) {
        if (prod.lhs == nt) {
            if (isupper(prod.rhs[prod.rhs.size() - 1])) {
                set<char> follow_set = follow(prod.lhs, grammar);
                trail_set.insert(follow_set.begin(), follow_set.end());
            } else if (prod.rhs[prod.rhs.size() - 1] != EPSILON) {
                trail_set.insert(prod.rhs[prod.rhs.size() - 1]);
            }
        }
    }
    return trail_set;
}

// Compute the first set of a string of symbols in a grammar
set<char> first(const string& symbols, const vector<Production>& grammar) {
    set<char> first_set;
    if (symbols.size() == 0) {
        first_set.insert(EPSILON);
    } else if (isupper(symbols[0])) {
        set<char> non_term_first_set = leading(symbols[0], grammar);
        if (non_term_first_set.find(EPSILON) != non_term_first_set.end()) {
            for (int i = 1; i < symbols.size(); i++) {
                set<char> next_first_set = first(string(1, symbols[i]), grammar);
                non_term_first_set.erase(EPSILON);
                non_term_first_set.insert(next_first_set.begin(), next_first_set.end());
                if (next_first_set.find(EPSILON) == next_first_set.end()) {
                    break;
                }
            }
        }
        first_set.insert(non_term_first_set.begin(), non_term_first_set.end());
    } else {
        first_set.insert(symbols[0]);
    }
    return first_set;
}
